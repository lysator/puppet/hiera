#!/bin/bash
set -euo pipefail

usage () {
    echo "Usage: ${0##*/} <FQDN>"
    exit
}

if [ "$#" -ne 1 ]; then
  usage
fi

for arg; do
  if [ "$arg" = "-h" ] || [ "$arg" = "--help" ]; then
    usage
  fi
done

REQUIRED_TOOLS=(ipa pwgen ipa-getkeytab mkpasswd tee)
MISSING_TOOLS=()
for tool in "${REQUIRED_TOOLS[@]}"; do
  if ! command -v "$tool" > /dev/null; then
    MISSING_TOOLS+=("$tool")
  fi
done

if [ -x /opt/puppetlabs/puppet/bin/eyaml ]; then
  EYAML=/opt/puppetlabs/puppet/bin/eyaml
elif command -v eyaml >/dev/null; then
  EYAML=eyaml
else
  MISSING_TOOLS+=(eyaml)
fi

if [ "${#MISSING_TOOLS[@]}" -gt 0 ]; then
  echo "ERROR: The following required tools are missing: ${MISSING_TOOLS[*]}"
  exit 1
fi

# Silence standard messages but not errors
# (ipa ping prints "IPA server version X. API version Y" on success)
ipa ping > /dev/null || { printf "\nFreeIPA connection check (ipa ping) failed. Do you have a valid kerberos ticket? (run klist to check, or kinit get a new ticket). See previous errors for details.\n" | fold --spaces; exit 1; }

cd "${BASH_SOURCE[0]%/*}" || { echo "Failed to change working directory to script dir"; exit 1; }

TEMP_DIR=$(mktemp -d /dev/shm/new_host.XXXXXXXX)
cleanup () {
  find "$TEMP_DIR" -type f -exec shred --force --zero {} +
  rm -rf "${TEMP_DIR}"
}
trap cleanup EXIT

FQDN=$1

# Add the host
ipa host-add "${FQDN}" || { echo "Failed to create host ${FQDN}"; exit 1; }
ipa netgroup-add-member lysnet --hosts="${FQDN}" || { echo "Failed to add ${FQDN} to lysnet net group"; exit 1; }
ipa-getkeytab -p "host/${FQDN}@AD.LYSATOR.LIU.SE" -k "${TEMP_DIR}/${FQDN}.keytab" || { echo "Failed to generate keytab"; exit 1; }
"$EYAML" encrypt --pkcs7-public-key=public_key.pkcs7.pem --output=block --label=ipa_client::keytab_content --file="${TEMP_DIR}/${FQDN}.keytab" | tee -a "nodes/${FQDN}.yaml"

ROOT_PASSWORD=$(pwgen -s 16 1)
echo "${ROOT_PASSWORD}" | mkpasswd -R 2000000 -s -m sha-512 | "$EYAML" encrypt --pkcs7-public-key=public_key.pkcs7.pem --output=block --label=profiles::root::password --stdin | tee -a "nodes/${FQDN}.yaml"
echo ""
echo "The output above has been saved to nodes/${FQDN}.yaml." 
echo "Please commit the file and push it."
echo ""
echo -e "\e[32mGenerated password for ${FQDN}:\e[39m ${ROOT_PASSWORD}"
echo -e "\e[35mSave the password or delete the entry from nodes/${FQDN}.yaml\e[39m"
